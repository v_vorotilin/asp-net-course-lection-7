using Microsoft.AspNetCore.Mvc;

namespace Lecture7 {

    public class Animal {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    
    public interface IRepository {
        Animal Get(int id);
        Animal[] GetAll();
        void Add(Animal animal);
    }

    public class Repository : IRepository {
        public Animal Get(int id) {
            if (id > 10)
                return null;
            
            return new Animal { Id = id, Name = "Cat" };
        }

        public Animal[] GetAll() {
            return new[] {
                new Animal { Id = 1, Name = "Cat" },
                new Animal { Id = 2, Name = "Dog" },
                new Animal { Id = 3, Name = "Parrot" },
            };
        }

        public void Add(Animal animal) {
            throw new System.NotImplementedException();
        }
    }
    
    [ApiController]
    [Route("api/[controller]")]
    public class AnimalController : ControllerBase {
        private readonly IRepository _repository;
        
        public AnimalController(IRepository repository) {
            _repository = repository;
        }
        
        [HttpGet("{id?}")]
        public IActionResult Get([FromRoute] int? id) {
            if (!id.HasValue)
                return new JsonResult(_repository.GetAll());

            var animal = _repository.Get(id.Value);

            if (animal == null)
                return NotFound();
            
            return new JsonResult(animal);
        }
    }
}