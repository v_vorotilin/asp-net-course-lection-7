﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Lecture7
{
    public class DatabaseConnectionInfo {
        public string Host { get; set; }
        public string DbName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
    
    public class Startup {
        private readonly IConfiguration _configuration;
        
        public Startup(IConfiguration configuration) {
            _configuration = configuration;
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services) {
            services.Configure<DatabaseConnectionInfo>(_configuration.GetSection("Database"));

            services.AddMvc();
            
            services.AddScoped<IRepository, Repository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc(routes => { routes.MapRoute("myRoute", "{controller}/{action}/{id?}"); });

//            using (var connection = new SqlConnection($"Host={dbInfo.Host};" +
//                                                      $"Initial Directory={dbInfo.DbName};" +
//                                                      $"User ID={dbInfo.Username};" +
//                                                      $"Password={dbInfo.Password}")) {
//                connection.Open();
//
//                using (var command = new SqlCommand("select * from Animal")) {
//                    using (var reader = command.ExecuteReader()) {
//                        Console.WriteLine(reader["Name"]);
//                    }
//                }
//            }

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Hello World!");
            });
        }
    }
}
